/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab03;

import static com.mycompany.lab03.Lab03.table;

/**
 *
 * @author User
 */
class XOProgram {

    static boolean checkWinRow(String[] table, String currentPlayer) {
        if (checkRow1_ByX(table)) {
            return true;
        } else if (checkRow1_ByO(table)) {
            return true;
        } else if (checkRow2_ByX(table)) {
            return true;
        } else if (checkRow2_ByO(table)) {
            return true;
        } else if (checkRow3_ByX(table)) {
            return true;
        } else if (checkRow3_ByO(table)) {
            return true;
        }
        return false;
    }

    private static boolean checkRow1_ByX(String[] table) {
        return table[0] == "X" && table[1] == "X" && table[2] == "X";
    }

    private static boolean checkRow1_ByO(String[] table) {
        return table[0] == "O" && table[1] == "O" && table[2] == "O";
    }

    private static boolean checkRow2_ByX(String[] table) {
        return table[3] == "X" && table[4] == "X" && table[5] == "X";
    }

    private static boolean checkRow2_ByO(String[] table) {
        return table[3] == "O" && table[4] == "O" && table[5] == "O";
    }

    private static boolean checkRow3_ByX(String[] table) {
        return table[6] == "X" && table[7] == "X" && table[8] == "X";
    }

    private static boolean checkRow3_ByO(String[] table) {
        return table[6] == "O" && table[7] == "O" && table[8] == "O";
    }

    static Object checkWinCol(String[] table, String currentPlayer) {
        if (checkCol1_ByX(table)) {
            return true;
        } else if (checkCol1_ByO(table)) {
            return true;
        } else if (checkCol2_ByX(table)) {
            return true;
        } else if (checkCol2_ByO(table)) {
            return true;
        } else if (checkCol3_ByX(table)) {
            return true;
        } else if (checkCol3_ByO(table)) {
            return true;
        }
        return false;
    }

    private static boolean checkCol1_ByX(String[] table) {
        return table[0] == "X" && table[3] == "X" && table[6] == "X";
    }

    private static boolean checkCol1_ByO(String[] table) {
        return table[0] == "O" && table[3] == "O" && table[6] == "O";
    }

    private static boolean checkCol2_ByX(String[] table) {
        return table[1] == "X" && table[4] == "X" && table[7] == "X";
    }

    private static boolean checkCol2_ByO(String[] table) {
        return table[1] == "O" && table[4] == "O" && table[7] == "O";
    }

    private static boolean checkCol3_ByX(String[] table) {
        return table[2] == "X" && table[5] == "X" && table[8] == "X";
    }

    private static boolean checkCol3_ByO(String[] table) {
        return table[2] == "O" && table[5] == "O" && table[8] == "O";
    }

    static Object checkWinDiagonal(String[] table, String currentPlayer) {
        if (checkDiagonal1_ByX(table)) {
            return true;
        } else if (checkDiagonal1_ByO(table)) {
            return true;
        } else if (checkDiagonal2_ByX(table)) {
            return true;
        } else if (checkDiagonal2_ByO(table)) {
            return true;
        }
        return false;
    }

    private static boolean checkDiagonal1_ByX(String[] table) {
        return table[0] == "X" && table[4] == "X" && table[8] == "X";
    }

    private static boolean checkDiagonal1_ByO(String[] table) {
        return table[0] == "O" && table[4] == "O" && table[8] == "O";
    }

    private static boolean checkDiagonal2_ByX(String[] table) {
        return table[2] == "X" && table[4] == "X" && table[6] == "X";
    }

    private static boolean checkDiagonal2_ByO(String[] table) {
        return table[2] == "O" && table[4] == "O" && table[6] == "O";
    }

    static Object CheckDraw(String[] table, String currentPlayer) {
        if (checkRow1_ByX(table)) {
            return true;
        } else if (checkRow1_ByO(table)) {
            return true;
        } else if (checkRow2_ByX(table)) {
            return true;
        } else if (checkRow2_ByO(table)) {
            return true;
        } else if (checkRow3_ByX(table)) {
            return true;
        } else if (checkRow3_ByO(table)) {
            return true;
        } else if (checkCol1_ByX(table)) {
            return true;
        } else if (checkCol1_ByO(table)) {
            return true;
        } else if (checkCol2_ByX(table)) {
            return true;
        } else if (checkCol2_ByO(table)) {
            return true;
        } else if (checkCol3_ByX(table)) {
            return true;
        } else if (checkCol3_ByO(table)) {
            return true;
        } else if (checkDiagonal1_ByX(table)) {
            return true;
        } else if (checkDiagonal1_ByO(table)) {
            return true;
        } else if (checkDiagonal2_ByX(table)) {
            return true;
        } else if (checkDiagonal2_ByO(table)) {
            return true;
        }
        return false;
    }

    static Object CheckNO_Player(String[] table, String currentPlayer) {
        for (int i = 0; i <= table.length; i++) {
            if(table[i] == "1" || table[i] == "2" || table[i] == "3" || table[i] == "4" || table[i] == "5" || table[i] == "6" || table[i] == "7" || table[i] == "8" || table[i] == "9"){
                return true;
            }
        }
        return false;
    }

}
