/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab03;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class TestLab03 {
    
    public TestLab03() {
    }

    @BeforeAll
    public static void setUpClass() throws Exception {
    }

    @AfterAll
    public static void tearDownClass() throws Exception {
    }

    @BeforeEach
    public void setUp() throws Exception {
    }

    @AfterEach
    public void tearDown() throws Exception {
    }
    
    

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public  void test_CheckWinRow1_ByX_True(){
        String[] table = {"X", "X", "X", "4", "5", "6", "7", "8", "9"};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWinRow(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinRow1_ByO_True(){
        String[] table = {"O", "O", "O", "4", "5", "6", "7", "8", "9"};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWinRow(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinRow2_ByX_True(){
        String[] table = {"1", "2", "3", "X", "X", "X", "7", "8", "9"};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWinRow(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinRow2_ByO_True(){
        String[] table = {"1", "2", "3", "O", "O", "O", "7", "8", "9"};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWinRow(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinRow3_ByX_True(){
        String[] table = {"1", "2", "3", "4", "5", "6", "X", "X", "X"};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWinRow(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinRow3_ByO_True(){
        String[] table = {"1", "2", "3", "4", "5", "6", "O", "O", "O"};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWinRow(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinCol1_ByX_True(){
        String[] table = {"X", "2", "3", "X", "5", "6", "X", "8", "9"};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWinCol(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinCol1_ByO_True(){
        String[] table = {"O", "2", "3", "O", "5", "6", "O", "8", "9"};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWinCol(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinCol2_ByX_True(){
        String[] table = {"1", "X", "3", "4", "X", "6", "7", "X", "9"};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWinCol(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinCol2_ByO_True(){
        String[] table = {"1", "O", "3", "4", "O", "6", "7", "O", "9"};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWinCol(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinCol3_ByX_True(){
        String[] table = {"1", "2", "X", "4", "5", "X", "7", "8", "X"};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWinCol(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinCol3_ByO_True(){
        String[] table = {"1", "2", "O", "4", "5", "O", "7", "8", "O"};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWinCol(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinDiagonal1_ByX_True(){
        String[] table = {"X", "2", "3", "4", "X", "6", "7", "8", "X"};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWinDiagonal(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinDiagonal1_ByO_True(){
        String[] table = {"O", "2", "3", "4", "O", "6", "7", "8", "O"};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWinDiagonal(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinDiagonal2_ByX_True(){
        String[] table = {"1", "2", "X", "4", "X", "6", "X", "8", "9"};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.checkWinDiagonal(table,currentPlayer));
    }
    @Test
    public  void test_CheckWinDiagonal2_ByO_True(){
        String[] table = {"1", "2", "O", "4", "O", "6", "O", "8", "9"};
        String currentPlayer = "O";
        assertEquals(true, XOProgram.checkWinDiagonal(table,currentPlayer));
    }
    @Test
    public  void test_CheckDraw1_False(){
        String[] table = {"X", "X", "O", "O", "X", "X", "X", "O", "O"};
        String currentPlayer = "";
        assertEquals(false, XOProgram.CheckDraw(table,currentPlayer));
    }
    @Test
    public  void test_CheckDraw2_False(){
        String[] table = {"O", "X", "O", "O", "X", "X", "X", "O", "O"};
        String currentPlayer = "";
        assertEquals(false, XOProgram.CheckDraw(table,currentPlayer));
    }
    @Test
    public  void test_CheckDraw3_False(){
        String[] table = {"O", "X", "O", "X", "O", "X", "X", "O", "X"};
        String currentPlayer = "";
        assertEquals(false, XOProgram.CheckDraw(table,currentPlayer));
    }
    @Test
    public  void test_CheckNO_Player(){
        String[] table = {"1", "2", "3", "4", "5", "6", "7", "8", "9"};
        String currentPlayer = "X";
        assertEquals(true, XOProgram.CheckNO_Player(table,currentPlayer));
    }
}                                                                                   
